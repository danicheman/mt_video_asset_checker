var request     = require('request');
var fs          = require('fs');
var lUtil    = require('./latinise');

function cdnUtil () {

    this.whitespaceRegex = new RegExp('/[^'+separator+'a-z0-9\s]+/');
    this.addDashesRegex = new RegExp('/['+separator+'\s]+/');

    this.getImage = function() {
        request('http://google.com/doodle.png').pipe(fs.createWriteStream('doodle.png'));
    }


    this.url_title = function(title)
    {
        separator = '-';

        // Replace accented characters by their unaccented equivalents
        //title = _transliterate_to_ascii($title);
        title = lUtil.latinize(title);


        // Remove all characters that are not the separator, a-z, 0-9, or whitespace
        title = title.replace(this.whitespaceRegex, '');


        // Replace all separator characters and whitespace by a single separator
        title = title.replace(this.addDashesRegex, separator);

        // Trim separators from the beginning and end
        return title.trim(separator);
    }
}

module.exports = cdnUtil;

